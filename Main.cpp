#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>

int main()
{
	// Gets the day of the month
	time_t now = time(NULL);
	tm* ltm = localtime(&now);
	int currentDay = ltm->tm_mday;

	// Sets constant values for the array
	const int a = 8;
	const int b = 6;

	// Gets the rest of the division number by date
	int N, restNum, sum{ 0 };
	std::cout << "Enter the N: ";
	std::cin >> N;
	restNum = currentDay % N;

	// Ctrate the array
	int array[a][b];
	std::cout << "Array created!" << std::endl;

	// Filling the array values
	for (int i = 0; i < a; i++)
	{
		for (int j = 0; j < b; j++)
		{
			array[i][j] = i + j;
		}
	}
	std::cout << "Array filled!" << std::endl;

	// Prints the array by row
	std::cout << "Array:\n{" << std::endl;
	for (int i = 0; i < a; i++)
	{
		std::cout << "  {";
		for (int j = 0; j < b; j++)
		{
			std::cout << array[i][j];

			if ( j != (b - 1) )
			{
				std::cout << ", ";
			}
		}
		std::cout << "}\n";
	}
	std::cout << "}\n" << std::endl;

	
	// Prints sum of row by date
	for (int i = 0; i < a; i++)
	{
		if (i == restNum)
		{
			for (int j = 0; j < b; j++)
			{
				sum += array[i][j];
			}
		}
	}
	std::cout << "Sum of row by date: " << sum;


	return 0;
}
